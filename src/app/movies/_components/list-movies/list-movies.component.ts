import { Component, OnInit,Input } from '@angular/core';
import { Movies } from 'src/app/_interfaces/movies';
import {CONFIG } from '../../../_services/configuration';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MoviesService } from '../../../_services/movies.service';
@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.scss']
})
export class ListMoviesComponent implements OnInit {
  @Input() typeRut: string;
  movies:Movies[];
  constructor(private _moviesService:MoviesService) { }

  ngOnInit(): void {
    this.getPopular();
  }
  getPopular(){
    this._moviesService.getCategory(this.typeRut).subscribe(
      resp=>{
        this.movies = resp['results'];
      }
    )
  }
}
