import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-visor',
  templateUrl: './visor.component.html',
  styleUrls: ['./visor.component.scss']
})
export class VisorComponent implements OnInit {
  vectNamesCategory : object[]=[];
  constructor() { 
    this.vectNamesCategory=[
      {"name":"Popular","type":"popular"},
      {"name":"Top rated","type":"top_rated"},
      {"name":"Upcoming","type":"upcoming"}
    ]

  }

  ngOnInit(): void {
    
  }

  

}
