import { Component, OnInit, Input } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MoviesService } from '../../../_services/movies.service';
import { Movies } from '../../../_interfaces/movies';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
  @Input() movieRestList:Object[];
  @Input() searchValue:string;
  public moviesList : Movies;
  public type:string;
  
  public pageSize:number;
  public pageNumber:number;
  public totalResults:number;
  constructor(public _moviesService:MoviesService) { 
    this.pageSize = 20;
    this.pageNumber = 1;
    this.type = "movie";
    
  
  }

  ngOnInit(): void {
   
  }

  changePaginate(list:any){
    this.movieRestList = list;
  }

  

}
