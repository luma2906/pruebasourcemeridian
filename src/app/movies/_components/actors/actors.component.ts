import { Component, OnInit,Input } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MoviesService } from '../../../_services/movies.service';
import { Actors } from '../../../_interfaces/actors';
import { Router } from '@angular/router';
import { Movies } from '../../../_interfaces/movies';

@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.scss']
})
export class ActorsComponent implements OnInit { 

  @Input() actorsRestList:Actors[];
  @Input() searchValue:string;
  public type:string;
  public pageSize:number;
  public pageNumber:number;
  public totalResults:number;
  constructor(public _moviesService:MoviesService, public ruta:Router) {
    this.type = "actors";
  }
  ngOnInit(): void {
  } 
  changePaginate(list:any){
    this.actorsRestList = list;
  }
  searchMovieByActor(idActor:number){
      this._moviesService.getSearchMovieByActor(idActor,this.pageNumber).subscribe(
        movieResult=>{ 
          movieResult['results'] = movieResult['cast'];
          movieResult['page'] = 1;
          movieResult['total_pages'] = 1;
          movieResult['total_results'] = movieResult['cast'].length;
          movieResult['results'].sort(this.orderMovieDate);
          this._moviesService.dispachSearchsMovies.emit(
            {movies:movieResult,
             actors:[],
             value:'amor',
            });  
          this.ruta.navigate(['listSearchsMovies']);  
      })
  }

  private orderMovieDate(movie1: Movies, movie2:Movies):number {
    let d1:any = new Date(movie1.release_date);
    let d2:any = new Date(movie2.release_date);
    return d1-d2;
  }
}
