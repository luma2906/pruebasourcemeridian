import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Actors } from 'src/app/_interfaces/actors';
import { Movies } from '../../../_interfaces/movies';
import { MoviesService } from '../../../_services/movies.service';

@Component({ 
  selector: 'app-list-movies-searchs',
  templateUrl: './list-movies-searchs.component.html',
  styleUrls: ['./list-movies-searchs.component.scss']
})
export class ListMoviesSearchsComponent implements OnInit {
  public movies:Object[];
  public actors:Object[];
  public codigo:string;
  public showMovies;
  public showActors;
  public value;
  public listMovies:Movies[];
  public listActors:Actors[];
  constructor(private _moviesService:MoviesService, private parametros:ActivatedRoute) { 
    this.codigo = parametros.snapshot.params.codigo;   
    this.showMovies = true;
    this.showActors = false;
    

  }

  ngOnInit(): void {
    this._moviesService.dispachSearchsMovies.subscribe(
      resp=>{
        this.movies = resp.movies;
        this.actors = resp.actors;
        this.value  = resp.value; 
        this.listMovies = resp.movies.results;
        this.listActors = resp.actors.results; 
        this.validate();
      }
    )
  }

  /**
   * function validate request of server
  */
  private validate(){
    this.actors['total_results'] = this.actors['total_results'] || 0;
    this.movies['total_results'] = this.movies['total_results'] || 0;
    if(this.movies['total_results'] > this.actors['total_results'] ){
      this.showMovies = true;
      this.showActors = false;    
    }else {   
      this.showMovies = false;
      this.showActors = true;
    }
  }

}
