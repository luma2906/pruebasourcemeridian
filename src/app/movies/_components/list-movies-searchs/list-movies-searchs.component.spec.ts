import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMoviesSearchsComponent } from './list-movies-searchs.component';

describe('ListMoviesSearchsComponent', () => {
  let component: ListMoviesSearchsComponent;
  let fixture: ComponentFixture<ListMoviesSearchsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListMoviesSearchsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMoviesSearchsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
