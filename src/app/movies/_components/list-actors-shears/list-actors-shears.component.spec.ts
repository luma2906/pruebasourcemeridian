import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListActorsShearsComponent } from './list-actors-shears.component';

describe('ListActorsShearsComponent', () => {
  let component: ListActorsShearsComponent;
  let fixture: ComponentFixture<ListActorsShearsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListActorsShearsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListActorsShearsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
