import { HttpClient } from '@angular/common/http';
import { Injectable, Output,EventEmitter  } from '@angular/core';
import { Observable } from 'rxjs';
import { Actors } from '../_interfaces/actors';
import { Movies } from '../_interfaces/movies';
import { Popular } from '../_interfaces/popular';
import { CONFIG } from './configuration';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  @Output() dispachSearchsMovies: EventEmitter<object> = new EventEmitter();
  @Output() dispachSearchsMoviesPages: EventEmitter<number> = new EventEmitter();
  public url: string;
  public apiKey : string;

  constructor(private http:HttpClient){
    this.url = CONFIG.urlApi;
    this.apiKey = CONFIG.apiKey;
  }
  getCategory(typeRut:string): Observable<Movies[]>{
    return this.http.get<Movies[]>(this.url+'movie/'+typeRut+'?api_key='+this.apiKey);
  }

  getSearchsMovies(filter:string,page = 1): Observable<Object[]>{    
    return this.http.get<Movies[]>(this.url+'search/movie'+'?api_key='+this.apiKey+'&query='+filter+'&page='+page);
  }

  getSearchsActor(filter:string,page = 1): Observable<Actors[]>{    
    return this.http.get<Actors[]>(this.url+'search/person'+'?api_key='+this.apiKey+'&query='+filter+'&page='+page);
  }
  
  getSearchMovieByActor(idActor:number,page = 1): Observable<Object[]>{ 
    return this.http.get<Movies[]>(this.url+'person/'+idActor+'/movie_credits'+'?api_key='+this.apiKey+'&page='+page);
  }

}
