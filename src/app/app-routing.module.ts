import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisorComponent } from '../app/movies/_components/visor/visor.component';
import { ListMoviesSearchsComponent } from '../app/movies/_components/list-movies-searchs/list-movies-searchs.component';

const routes: Routes = [
  {path:'',component:VisorComponent},
  {path:'listSearchsMovies',component:ListMoviesSearchsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
