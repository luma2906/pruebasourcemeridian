import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule} from'@angular/forms';
import { HttpClientModule } from '@angular/common/http';



/**
 * Components
 */
import { CardsComponent } from './shared/_components/cards/cards.component';
import { MenuTopComponent } from './layout/_components/menu-top/menu-top.component';
import { SearchComponent } from './shared/_components/search/search.component';
import { VisorComponent } from './movies/_components/visor/visor.component';
import { ListMoviesComponent } from './movies/_components/list-movies/list-movies.component';
import { InformationComponent } from './layout/_components/information/information.component';
import { MainComponent } from './layout/_components/main/main.component';
import { ListMoviesSearchsComponent } from './movies/_components/list-movies-searchs/list-movies-searchs.component';
import { ListActorsShearsComponent } from './movies/_components/list-actors-shears/list-actors-shears.component';
import { ActorsComponent } from './movies/_components/actors/actors.component';
import { MoviesComponent } from './movies/_components/movies/movies.component';
import { CardsPersonComponent } from './shared/_components/cards-person/cards-person.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FlexLayoutModule } from '@angular/flex-layout';

import { PaginatePipe } from './pipes/paginate.pipe';
import { PaginationComponent } from './shared/_components/pagination/pagination.component';


/**
 * modules
 */
 import { MaterialModuleModule } from './material-module/material-module.module';

@NgModule({
  declarations: [
    AppComponent,
    MenuTopComponent,
    SearchComponent,
    CardsComponent,
    VisorComponent,
    ListMoviesComponent,
    InformationComponent,
    MainComponent,
    ListMoviesSearchsComponent,
    ListActorsShearsComponent,
    ActorsComponent,
    MoviesComponent,
    CardsPersonComponent,
    PaginatePipe,
    PaginationComponent,
    
   

  ],
  imports: [    
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModuleModule,
    FormsModule, 
    ReactiveFormsModule,   
    HttpClientModule,
    MatPaginatorModule,
    FlexLayoutModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
 
})
export class AppModule { }
