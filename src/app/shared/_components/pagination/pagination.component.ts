import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MoviesService } from '../../../_services/movies.service';
import { Movies } from '../../../_interfaces/movies';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  public moviesList : Movies;  
  public pageSize:number;
  public pageNumber:number;
  public totalResults:number;  
  @Input() size:number;
  @Input() pageSizes:number;
  @Input() valueSearchs:string;
  @Input() type:string;
  @Output() changePaginate:EventEmitter<any> = new EventEmitter();

  constructor(public _moviesService:MoviesService) { }

  ngOnInit(): void {
  }

  changePage(e : PageEvent){
    this.pageSize = e.pageSize;
    this.pageNumber = e.pageIndex+1;
    if(this.type === "movie"){
      this.getMovies();
    }else if(this.type === "actors"){
      this.getActors();
    }
    

  }
  private getMovies(){
    this._moviesService.getSearchsMovies(this.valueSearchs,this.pageNumber).subscribe(
      movieResult=>{  
        this.changePaginate.emit(movieResult); 
    })
  }

  private getActors(){    
    this._moviesService.getSearchsActor(this.valueSearchs,this.pageNumber).subscribe(
      movieResult=>{   
        this.changePaginate.emit(movieResult); 

    })
  }

}
