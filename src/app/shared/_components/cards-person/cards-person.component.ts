import { Component, OnInit, Input } from '@angular/core';
import { Actors } from 'src/app/_interfaces/actors';
import { CONFIG } from '../../../_services/configuration';
@Component({
  selector: 'app-cards-person',
  templateUrl: './cards-person.component.html',
  styleUrls: ['./cards-person.component.scss']
})
export class CardsPersonComponent implements OnInit {

 
  public basePath:String;
  public nombre:String;
  @Input() movieRest:Actors;
  constructor() { 
    this.basePath = CONFIG.baseImages;
  }
  ngOnInit(): void { 
  }
}
