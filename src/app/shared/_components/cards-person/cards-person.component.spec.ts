import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsPersonComponent } from './cards-person.component';

describe('CardsPersonComponent', () => {
  let component: CardsPersonComponent;
  let fixture: ComponentFixture<CardsPersonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardsPersonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
