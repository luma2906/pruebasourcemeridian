import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Movies } from 'src/app/_interfaces/movies';
import { MoviesService } from '../../../_services/movies.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  
  public form: FormGroup;
  constructor(private formBuilder:FormBuilder,
    private _moviesService:MoviesService,
    private ruta: Router) {
    this.form = new FormGroup({});
    this.crearFormulario();
  }

  ngOnInit(): void {

    this._moviesService.dispachSearchsMoviesPages.subscribe(page=>{
      this.getMoviesSearchs(page);
    });
  }
  public isShowInput = true;
  public movies:Movies;

  public showInput(): void {
    this.isShowInput = true;
  }
  public  buscar(){
    if(this.form.controls['search'].value!="" 
     && this.form.controls['search'].value!=null){
        this.getMoviesSearchs();
    }else{     
      this.ruta.navigate(['']); 
    }
  }

  crearFormulario(){
    this.form = this.formBuilder.group({
      search :['',[Validators.required]],
   
    })
  }

  getMoviesSearchs(page=1){
    this._moviesService.getSearchsMovies(this.form.controls['search'].value,page).subscribe(
      moviesResult=>{
        
        this._moviesService.getSearchsActor(this.form.controls['search'].value,page).subscribe(
          actorResult=>{   
            this._moviesService.dispachSearchsMovies.emit(
              {movies:moviesResult,
               actors:actorResult,
               value:this.form.controls['search'].value,
              });  
             
          })        
        }      
      )
      this.ruta.navigate(['listSearchsMovies']); 
  }
  public keyEnter(){
    this.buscar();
  }


  

}

